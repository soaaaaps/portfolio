import logo from './logo.svg';
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import './App.css';
import MainPage from './components/MainPage';
import About from './components/About';
import Education from './components/Education';
import Projects from './components/Projects';
import Contact from './components/Contact';
import Navbar from './components/Navbar';

function App() {
  return (
    <BrowserRouter>
    <Routes>
      <Route path='/' element={<MainPage />} />
      <Route path='about' element={<About />} />
      <Route path='education' element={<Education />} />
      <Route path='projects' element={<Projects />} />
      <Route path='contact' element={<Contact />} />
      <Route path='navbar' element={<Navbar />} />
    </Routes></BrowserRouter>
  );
}

export default App;
