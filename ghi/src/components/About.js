import React from 'react'
import Navbar from './Navbar'

export default function About() {
    return(
        <>
        <Navbar />
        <div className="centered">
        <div className="sectionTitle">
        <h1>About</h1>
        </div>
        <div className="sectionAbout">
        <p>Lollipop croissant powder wafer bear claw oat cake shortbread. Marzipan gingerbread marshmallow soufflé candy canes. Macaroon cookie sugar plum brownie I love lemon drops I love sweet roll tootsie roll. Donut fruitcake muffin tart wafer. Jujubes gummies pudding pastry gummi bears. Tootsie roll wafer I love jelly-o marshmallow croissant chocolate bar. Lemon drops marzipan powder I love carrot cake.</p>
        </div>
        <div className="sectionTitle">
        <h1>Skills</h1>
        </div>
        <div className="sectionAbout">
        <img src="images/gitlab.png" width="50" />
        <img src="images/python.ico" width="50" />
        <img src="images/javascript.png" width="50" />
        <img src="logo512.png" width="50" />
        <img src="images/django.png" width="50"/>
        <img src="images/posgresql.png" width="50"/>
        <img src="images/sql.png" width="50"/>
        <img src="images/html.png" width="50"/>
        <img src="images/css.png" width="50"/>
        <img src="images/fastapi.png" width="50"/>
        </div>
        </div>
        </>
    )
}
