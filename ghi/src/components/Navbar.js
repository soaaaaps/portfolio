import * as React from 'react';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import CottageOutlinedIcon from '@mui/icons-material/Cottage';
import SchoolOutlinedIcon from '@mui/icons-material/SchoolOutlined';
import EmojiPeopleOutlinedIcon from '@mui/icons-material/EmojiPeopleOutlined';
import BuildOutlinedIcon from '@mui/icons-material/BuildOutlined';
import EmailOutlinedIcon from '@mui/icons-material/EmailOutlined';
import Education from './Education';

export default function Navbar() {

    return (
    <Tabs aria-label="icon label tabs example" centered>
        <Tab href="/" icon={<CottageOutlinedIcon />} />
        <Tab href="/about" icon={<EmojiPeopleOutlinedIcon />} />
        <Tab href="/education" icon={<SchoolOutlinedIcon />} />
        <Tab href="/projects" icon={<BuildOutlinedIcon />}  />
        <Tab href="/contact" icon={<EmailOutlinedIcon />}  />
      </Tabs>
    );
}
