import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import Typography from '@mui/material/Typography';
import Navbar from './Navbar';



export default function Education() {
    return (
        <>
        <Navbar />
        <div className="centered">
        <div className="sectionTitle">
        <h1>Education</h1>
        </div>
        <div className="sectionAbout">
            <Stack direction="row" spacing={2}>
                <Card sx={{ maxWidth: 345, borderRadius: '16px', boxShadow: 4}} variant="outlined">
                    <CardMedia
                        component="img"
                        alt="green iguana"
                        height="300"
                        image="images/hackreactor.png"
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="div">
                            <div>Hack Reactor</div>
                            <div>by Galvanize</div>
                        </Typography>
                        <Typography variant="body2" color="text.secondary">
                            Software Engineering Immersive with JavaScript and Python
                        </Typography>
                    </CardContent>
                    <CardActions>
                        <Button size="small">Learn More</Button>
                    </CardActions>
                </Card>
                <Card sx={{ maxWidth: 345,borderRadius: '16px', boxShadow: 4}} raised variant="outlined">
                    <CardMedia
                        component="img"
                        alt="green iguana"
                        height="300"
                        image="images/csulb.jpg"
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="div">
                            California State University, Long Beach
                        </Typography>
                        <Typography variant="body2" color="text.secondary">
                            Bachelor of Sciences in Business Administration, Accountancy
                        </Typography>
                    </CardContent>
                    <CardActions>
                        <Button size="small">Learn More</Button>
                    </CardActions>
                </Card>
            </Stack>
            </div>
            </div>
        </>

    )
}
