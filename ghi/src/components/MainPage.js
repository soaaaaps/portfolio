import React from 'react';
import "../stylesheet.css"
import Navbar from './Navbar';
import About from './About';



export default function MainPage() {
    return (
        <>
        <Navbar />
        <div class="container">
        <img src="images/cloud.png" width="800" />
        <div class="centered">
            <div>I'm Sophie,</div>
            <div>a Software Developer</div>
        </div>
        </div>
        </>
    )
}
